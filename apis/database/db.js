const config = require('../config');
var dbCfg = config.db;
// var Mongoose = require('mongoose');

// var myDB = Mongoose.connect(dbCfg.url)

// module.exports = myDB;
// var neo4j = require('node-neo4j');
// myDB = new neo4j(`http://${dbCfg.user}:${dbCfg.password}@${dbCfg.host}:${dbCfg.port}`);

const neo4j = require('neo4j-driver').v1;

const driver = neo4j.driver(dbCfg.url, neo4j.auth.basic(dbCfg.user, dbCfg.password));
const myDB = driver.session();


module.exports = myDB;
