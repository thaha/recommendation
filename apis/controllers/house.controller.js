const status = require('../services/sys/service.statuscode');
const sys = require('../services/sys/service.sys');
const DB = require('../database/db');
const code = status.status;
const reply = status.reply;

function list(req, res, next) {
    const resultPromise = DB.run(
        'MATCH (h:House) RETURN h'
    );

    resultPromise.then(results => {

        // const singleRecord = result.records[0];
        // const node = singleRecord.get(0);

        var houses = [];

        results['records'].map(function (result) {
            result['_fields'].map(function (result2) {
                return houses.push(result2['properties']);
            });
        });
        console.log(houses)

        reply(res, code.SUCCESS, houses);
    });
}

function create(req, res, next) {
    req.body.house.uuid = sys.uuid();
    const resultPromise = DB.run(
        'CREATE (h:House {uuid: $uuid, name: $name, lat: $lat, lng: $lng}) RETURN h',
        req.body.house
    );

    resultPromise.then(result => {
        const singleRecord = result.records[0];
        const node = singleRecord.get(0);

        reply(res, code.SUCCESS, node.properties);
    }).catch(ex => {
        console.log(ex);
        reply(res, code.BAD_REQ, req.body.house);
    });
}


function update(req, res, next) {
    let data = req.body.house;
    let house = {
        name: data.name,
        lat: data.lat,
        lng: data.lng,
        description: data.description,
        uuid: req.params.id
    };
    const resultPromise = DB.run(
        'MATCH (h:House {uuid: $uuid}) SET h=$house RETURN h',
        {uuid: req.params.id, house}
    );

    resultPromise.then(result => {
        const singleRecord = result.records[0];

        reply(res, code.SUCCESS, house);
    }).catch(ex => {
        console.log(ex);
        reply(res, code.BAD_REQ, req.body.house);
    });
}

function remove(req, res, next) {
    const resultPromise = DB.run(
        'MATCH (h:House {uuid: $id}) DETACH DELETE h',
        req.params
    );

    resultPromise.then(result => {
        reply(res, code.SUCCESS, { uuid: req.params.id });
    }).catch(ex => {
        console.log(ex);
        reply(res, code.BAD_REQ, {});
    });
}

module.exports = {
    list,
    create,
    update,
    remove,
};
