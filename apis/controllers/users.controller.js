const status = require('../services/sys/service.statuscode');
const auth = require('../services/auth/service.auth');
const code = status.status;
const reply = status.reply;

function signin(req, res, next) {
    auth.login(req).then((data) => {
        console.log(data);
        reply(res, code.SUCCESS, data);
    }).catch(ex => {
        console.log(ex);
        reply(res, code.BAD_AUTH, { auth: false, token: null });
    });
}

function authorize(req, res, next) {
    reply(res, code.SUCCESS, { auth: true });
}

function current(req, res, next) {
    reply(res, code.SUCCESS, { auth: true, token: 123 });
}

module.exports = {
    signin, 
    authorize, 
    current,
};
