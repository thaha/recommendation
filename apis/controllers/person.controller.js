const status = require('../services/sys/service.statuscode');
const Person = require('../models/sample.model');
const DB = require('../database/db');
const sys = require('../services/sys/service.sys');
const code = status.status;
const reply = status.reply;

function all(req, res, next) {
    DB.run(
        'MATCH (p:Person)-[r:MEMBER]->(h:House) RETURN p, h'
    ).then(results => {
        console.log(results)

        var person = [];

        results['records'].map(function (result) {
            let p = result['_fields'][0].properties;
            p.house = result['_fields'][1].properties;
            return person.push(p);
        });

        reply(res, code.SUCCESS, person);
    });
}

function list(req, res, next) {
    DB.run(
        'MATCH (p:Person {houseId: $id})-[r]->(parents) RETURN p, collect({relation: type(r), person: parents})',
        req.params
    ).then(results => {
        var person = [];

        results['records'].map(function (result) {
            let p = result['_fields'][0].properties;
            console.log(result['_fields'][1])

            p.PURCHASED = [];

            result['_fields'][1].map(function (result1) {
                if (result1.relation === 'PURCHASED') {
                    p.PURCHASED.push(result1.person.properties);
                } else {
                    p[result1.relation] = result1.person.properties;
                }
            });
            return person.push(p);
        });
        
        reply(res, code.SUCCESS, person);
    });
}

function create(req, res, next) {
    let newPerson = createPerson(req.body.person);

    reply(res, code.SUCCESS, newPerson);
}

function createPerson(person) {
    person.uuid = sys.uuid();

    DB.run(
        'CREATE (p:Person $person) RETURN p',
        {person}
    ).then(result1 => {
        const singleRecord = result1.records[0];
        const node = singleRecord.get(0);

        const newPerson = node.properties;

        DB.run(
            'MATCH (h:House),(p:Person) WHERE h.uuid = $houseId AND p.uuid = $uuid CREATE(p) - [r:MEMBER] -> (h) RETURN type(r)',
            person
        ).then(result => {
            console.log(result);
        });

        saveRelation(newPerson);
    });

    return person;
}

function updatePerson(person) {
    DB.run(
        'MATCH (p:Person {uuid: $uuid}) SET p = $person',
        {uuid: person.uuid, person}
    ).then(result1 => {
        saveRelation(person);
    });

    return person;
}

function update(req, res, next) {
    let person = req.body.person;
    
    removePersonRelation(person.uuid);
    let newPerson = updatePerson(person);
    reply(res, code.SUCCESS, newPerson);
}

function saveRelation(data) {
    if (data.father != '') {
        DB.run(
            'MATCH (h:Person),(p:Person) WHERE h.uuid = $father AND p.uuid = $uuid CREATE(p) - [r:FATHER] -> (h) RETURN type(r)',
            data
        ).then(result => {
            console.log(result);
        });
    } 
    if (data.mother != '') {
        DB.run(
            'MATCH (h:Person),(p:Person) WHERE h.uuid = $mother AND p.uuid = $uuid CREATE(p) - [r:MOTHER] -> (h) RETURN type(r)',
            data
        ).then(result => {
            console.log(result);
        });
    }
}

function remove(req, res, next) {
    removePerson(req.params.id);

    reply(res, code.SUCCESS, { uuid: req.params.id });

}

function removePerson(id) {

    const resultPromise = DB.run(
        'MATCH (p:Person {uuid: $id}) DETACH DELETE p',
        {id: id}
    );

    resultPromise.then(result => {
        console.log(result);
    }) 
}

function removePersonRelation(id) {
    DB.run(
        'MATCH (:Person {uuid: $id})-[r:FATHER]->(:Person) DELETE r',
        { id }
    ).then(result => {
        console.log(result);
    }); 
    DB.run(
        'MATCH (:Person {uuid: $id})-[r:MOTHER]->(:Person) DELETE r',
        { id }
    ).then(result => {
        console.log(result);
    }); 
}
Date.prototype.addDays = function (days) {
    this.setDate(this.getDate() + parseInt(days));
    return this;
};
function recommendations(req, res, next) {
    let maxDist = 1000; // 1 km
    var products = [];

    DB.run(
        `MATCH (p:Person {uuid: $id})-[]->(h:House) RETURN p, h`,
        { id: req.params.id }
    ).then(result01 => {
        let person = result01['records'][0]['_fields'][0].properties;
        person.house = result01['records'][0]['_fields'][1].properties;

        person.house.maxDist = maxDist;

        DB.run(
            `MATCH (h:House) 
            WHERE distance( point({ longitude: $lng, latitude: $lat, crs: 'WGS-84' }), point({ longitude: h.lng, latitude: h.lat,  crs: 'WGS-84' })) <= $maxDist
            RETURN collect(h.uuid), collect(distance( point({ longitude: $lng, latitude: $lat, crs: 'WGS-84' }), point({ longitude: h.lng, latitude: h.lat,  crs: 'WGS-84' })))`,
            person.house
        ).then(nearHouses => {
            let nearHouseIds = [];

            if (nearHouses['records'][0] !== undefined) {
                nearHouseIds = nearHouses['records'][0]['_fields'][0];
                console.log(nearHouses['records'][0]['_fields'][1])
            }

            person.nearHouseIds = nearHouseIds;

            DB.run(
                `MATCH (:Person {uuid: $uuid})-[*1]->(:Person)<-[*1]-(by:Person)-[:PURCHASED]->(product:Product) 
                WHERE NOT by.uuid = $uuid AND by.gender = $gender
                RETURN {product: product, by: by, relation: 'Sibling'}`,
                person
            ).then(results1 => {
                saveRecords(products, results1['records']);

                DB.run(
                    `MATCH(: Person { uuid: $uuid }) - [*2] -> (:Person) < -[*2] - (by:Person) - [: PURCHASED] -> (product: Product) 
                    WHERE NOT by.uuid = $uuid AND by.gender = $gender
                    RETURN {product: product, by: by, relation: 'Cousin'}`,
                    person
                ).then(results2 => {
                    saveRecords(products, results2['records']);

                    DB.run(
                        `MATCH(p:Person)-[:PURCHASED]->(product: Product) 
                        WITH date(p.dob) as dob, product as product, p as p
                        WHERE p.houseId IN $nearHouseIds 
                            AND NOT p.uuid = $uuid AND p.gender = $gender 
                            AND date(p.dob) +  duration("P5Y") > date($dob) 
                            AND date(p.dob) -  duration("P5Y") < date($dob) 
                        RETURN {product: product, by: p, relation: 'Neighbour'}`,
                        person
                    ).then(results3 => {
                        saveRecords(products, results3['records']);

                        reply(res, code.SUCCESS, products);
                    });
                });
            });
        });
    });
}

function saveRecords(arr, data) {

    data.map(function (result) {
        let p = result['_fields'][0].product.properties;
        p.by = result['_fields'][0].by.properties;
        p.relation = result['_fields'][0].relation;
        
        if (arr.findIndex((a) => a.uuid == p.uuid) === -1) {
            arr.push(p);
        } 

        return true;
    });

    return arr

}

module.exports = {
    all,
    list,
    create,
    update,
    remove,
    recommendations,
};
