const status = require('../services/sys/service.statuscode');
const Person = require('../models/sample.model');
const DB = require('../database/db');
const sys = require('../services/sys/service.sys');
const code = status.status;
const reply = status.reply;


function list(req, res, next) {
    DB.run(
        'MATCH (p:Product) RETURN p'
    ).then(results => {
        var products = [];

        results['records'].map(function (result) {
            result['_fields'].map(function (result2) {
                return products.push(result2['properties']);
            });
        });

        reply(res, code.SUCCESS, products);
    });
}

function buy(req, res, next) {
    DB.run(
        'MATCH (product:Product),(person:Person) WHERE product.uuid = $id AND person.uuid = $personId CREATE UNIQUE (person) - [:PURCHASED {quantity: 1}] -> (product) RETURN person,product',
        { id: req.params.id, personId: req.body.person.uuid }
    ).then(results => {
        reply(res, code.SUCCESS, req.body.person);
    });
}


module.exports = {
    list,
    buy,
};
