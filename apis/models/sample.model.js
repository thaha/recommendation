var Mongoose = require('mongoose');
var Schema = Mongoose.Schema;

var sampleSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    description: String,
    deleted: {
        type: Boolean,
        default: false,
    },
    location: {
        addressComponents: [{
            long_name: String,
            short_name: String,
            types: [String],
        }],
        placeId: String,
        formattedAddress: String,
        lat: Number,
        lng: Number,
        types: [String],
    },
}, {
    timestamps: true
});

var Sample = Mongoose.model('Sample', sampleSchema);

module.exports = Sample;
