const DB = require('../database/db');

const sys = require('../services/sys/service.sys');

const products = [
    { name: 'Moto C Plus', price: 6000 },
    { name: 'H4 Mini Dome Camera', price: 2000},
]

DB.run(
    'MATCH (p:Product) DETACH DELETE p'
).then(result => {
    console.log("deleted", result)
});

for(product of products) {
    product.uuid = sys.uuid();

    DB.run(
        'CREATE (p:Product $product) RETURN p',
        {product}
    ).then(result => {
        console.log("created", result)
        return true;
    });
}

return true;
