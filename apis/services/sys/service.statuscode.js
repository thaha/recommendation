var uuidv4 = require('uuid/v4');

exports.status = {
    LOGIN_SUCCESS: {
        code: 200,
        status: 'LOGIN_SUCCESS',
        message: 'Success'
    },
    TOKEN_VERIFIED: {
        code: 200,
        status: 'TOKEN_VERIFIED',
        message: 'Success'
    },
    SUCCESS: {
        code: 200,
        status: 'SUCCESS',
        message: 'Success'
    },
    BAD_REQ: {
        code: 400,
        status: 'BAD_REQ',
        message: 'Bad request'
    },
    BAD_AUTH: {
        code: 401,
        status: 'BAD_AUTH',
        message: 'Requires authentication'
    },
    UNAUTHORIZED: {
        code: 401,
        status: 'BAD_AUTH',
        message: 'Authentication failed'
    },
    TOO_MANY_REQ: {
        code: 429,
        status: 'TOO_MANY_REQ',
        message: 'Too many requests'
    },
    ERROR: {
        code: 500,
        status: 'ERROR',
        message: 'Request error'
    },
    NOT_FOUND: {
        code: 404,
        status: 'NOT_FOUND',
        message: 'Not Found'
    }
}

exports.reply = function (res, reply, data = null) {
    return res.status(reply.code).json(logData({
        code: reply.code,
        message: reply.message,
        status: reply.status,
        requestId: uuidv4(),
        data
    }));
}

function logData(data) {
    console.log(data);
    return data
}