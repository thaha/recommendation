const config = require('../../config');
const request = require('request');
const Promise = require('bluebird');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

module.exports.getToken = function (req) {
    if (req.headers.authorization) {
        var token = req.headers.authorization;
        return token;
    } else {
        if (req.query.token) {
            return req.query.token;
        } else {
            return null;
        }
    }
}

exports.verifyToken = function (req, res, next) {
    const token = this.getToken(req);

    return new Promise(function (resolve, reject) {

        jwt.verify(token, config.app.secret, function(err, decoded) {
            if (err) return reject('Unable to verify token');
            
            resolve(decoded);
        });

    });
}

exports.login = function (req) {
    const user = req.body.user;

    return new Promise(function (resolve, reject) {
        console.log(user);
        
        if (user.username !== config.app.email || user.password !== config.app.password) {
            return reject('Invalid email');
        }

        var token = jwt.sign(user, config.app.secret, {
            expiresIn: 864000 // expires in 10 days
        });
        
        return resolve({token: token})
    });
}
