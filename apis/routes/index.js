var express = require('express');
var auth = require('../services/auth/service.auth');
const status = require('../services/sys/service.statuscode');

const reply = status.reply;
const code = status.status;

const usersController = require('../controllers/users.controller');

const houseController = require('../controllers/house.controller');
const personController = require('../controllers/person.controller');
const productController = require('../controllers/product.controller');


const securedRoutes = express.Router();

/** GET /health-check - Check service health */
securedRoutes.get('/health-check', (req, res) =>
  res.send('OK')
);

securedRoutes.use((req, res, next) => {
  auth.verifyToken(req, res, next).then((data) => {
    next();
  }).catch(ex => {
    console.log(ex);
    reply(res, code.UNAUTHORIZED, {});
  });
});

securedRoutes.route('/users/authorize').post(usersController.authorize);
securedRoutes.route('/users/current').post(usersController.current);


securedRoutes.route('/person').get(personController.all);
securedRoutes.route('/house/:id/person').get(personController.list);
securedRoutes.route('/house/:id/person').post(personController.create);

securedRoutes.route('/house').get(houseController.list);
securedRoutes.route('/house').post(houseController.create);
securedRoutes.route('/house/:id').put(houseController.update);
securedRoutes.route('/house/:id').delete(houseController.remove);
securedRoutes.route('/person/:id').put(personController.update);
securedRoutes.route('/person/:id').delete(personController.remove);
securedRoutes.route('/person/:id/recommendations').get(personController.recommendations);

securedRoutes.route('/product').get(productController.list);
securedRoutes.route('/product/:id/buy').post(productController.buy);

module.exports = securedRoutes;
