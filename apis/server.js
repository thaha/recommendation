const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
var db = require('./database/db');

var cors = require('cors');

var routes = require('./routes');
var config = require('./config');
const usersController = require('./controllers/users.controller');

const app = express();

app.use(cors());

// Parsers
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE");
  next();
});

app.post('/api/users/sign_in', usersController.signin);

app.use('/api', routes);


// 404 and forward to error handler
app.use((req, res, next) => {
    res.status(404).json({code: 404});
});

var server = require('http').Server(app);

server.listen(config.port, config.ip, function(){
});
