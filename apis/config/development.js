module.exports = {
    db: {
        url: 'bolt://localhost:7687',
        user: 'neo4j',
        port: 7687,
        host: 'localhost',
        password: '1234'
    },

    ip: '0.0.0.0',
    port: '3001',

    app: {
        email: 'admin',
        password: 'admin',
        secret: 'test1',
    }
};
