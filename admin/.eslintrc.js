module.exports = {
    "extends": "airbnb",
    "plugins": [
        "react",
        "jsx-a11y"
    ],
    "globals": {
      "window": true,
      "document": true
    },
    "rules": {
      "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }]
    }
};
