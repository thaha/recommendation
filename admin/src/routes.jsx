import React from 'react';
import { 
    IndexRoute, Route
} from 'react-router';
import {Root} from './Root';
import Page404 from './common/404';
import LoginContainer from './auth';
import HomeContainer from './home';
// import { BrowserRouter as Router, Route,  } from 'react-router-dom';
export default (
    <Route>
        <Route path="/login" component={LoginContainer} />
        <Route path="/" component={Root}>
            <IndexRoute component={HomeContainer} />
        </Route>
        <Route path="*" component={Page404} />
    </Route>
);
