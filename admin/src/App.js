import React from 'react';
import { Router, hashHistory } from 'react-router';
import routes from './routes';
import './App.css';

let BASE_URL_API;
let MAP_API_KEY;

if (process.env.NODE_ENV === 'production') {
  BASE_URL_API = 'http://35.226.15.65:3001/api';
  MAP_API_KEY = 'AIzaSyDenEDUySELPNrWQ30WHO-k-LBvuOmj_wU';
} else {
  BASE_URL_API = 'http://0.0.0.0:3001/api';
  MAP_API_KEY = 'AIzaSyCJ8osy9FhJTvkz31u4PujAiDtEWKNf5t8';
}
export { BASE_URL_API, MAP_API_KEY };

export default function App() {
  return (<
    Router history={hashHistory}
    routes={routes}
  />)
}
