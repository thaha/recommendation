import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import compose from 'recompose/compose';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Delete from '@material-ui/icons/Delete';
import Cancel from '@material-ui/icons/Cancel';
import { deletePerson } from './HomeActions';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


class DeletePersonAlert extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
    this.handleClose = this.handleClose.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.open !== this.props.open) {
      this.setState({
        open: nextProps.open,
      });
    }
  }

  handleClose() {
    this.setState({ open: false });
    this.props.handleCloseDelete();
  }

  handleDelete() {
    const { dispatch } = this.props;
    const personId = this.props.person && this.props.person.uuid;
    dispatch(deletePerson(personId));
    this.handleClose();
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"Delete Person"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
            Are you sure you want to delete this person?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} className={classes.button} variant="raised" color="default">
              <div style={{ paddingTop: '3px' }}>Cancel</div>
              <Cancel className={classes.rightIcon} />
            </Button>
            <Button onClick={this.handleDelete} className={classes.buttonSecondary} variant="raised" color="secondary">
              <div style={{ paddingTop: '3px' }}>Delete</div>
              <Delete className={classes.rightIcon} />
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { HomeReducer } = state;
  const { persons } = HomeReducer;
  return { persons };
}

DeletePersonAlert.propTypes = {
  dispatch: PropTypes.func.isRequired,
  open: PropTypes.bool,
  person: PropTypes.object.isRequired,
  handleCloseDelete: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    backgroundColor: '#fff',
  },
  buttonSecondary: {
    margin: theme.spacing.unit,
    backgroundColor: '#dc3545',
    '&:hover': {
      backgroundColor: '#dc1f31',
    },
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
});

export default compose(withStyles(styles), connect(mapStateToProps))(DeletePersonAlert);
