import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import MenuItem from '@material-ui/core/MenuItem';
import Chip from '@material-ui/core/Chip';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormGroup from '@material-ui/core/FormGroup';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import FormLabel from '@material-ui/core/FormLabel';
import compose from 'recompose/compose';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Save from '@material-ui/icons/Save';
import Cancel from '@material-ui/icons/Cancel';
import Avatar from '@material-ui/core/Avatar';
import {
  addPersons, updatePersons,
} from './HomeActions';

class CreatePerson extends Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {};
    this.initState();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.person) {
      this.setPersonDetails(nextProps.person);
    }
    this.setState({
      open: nextProps.open,
    });
  }

  initState() {
    this.state = {
      open: this.props.open,
      name: '',
      description: '',
      mother: '',
      dob: '',
      dod: '',
      gender: 'MALE',
      father: '',
      relations: [
        { value: 'FATHER', label: 'Father' },
        { value: 'MOTHER', label: 'Mother' },
      ],
      genders: [
        { value: 'MALE', label: 'Male' },
        { value: 'FEMALE', label: 'Female' },
      ]
    };
  }

  setPersonDetails(person) {
    this.setState({
      name: person.name,
      description: person.description,
      uuid: person.uuid,
      dob: person.dob,
      dod: person.dod,
      gender: person.gender,
      father: person.father,
      mother: person.mother,
    });
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleClose() {
    this.setState({ open: false });
    this.props.modalClosed(true);
    this.initState();
  }

  handleOpen() {
    this.setState({ open: true });
  }

  isFormValid() {
    let valid = true;

    if (!this.state.name || this.state.name.length === 0) {
      valid = false;
      this.setState({
        nameError: 'Title is required',
      });
    }
    
    return valid;
  }

  handleSubmit() {
    if (this.isFormValid()) {
      const newPerson = {
        houseId: this.props.currentHouseId,
        name: this.state.name.trim(),
        description: this.state.description.trim(),
        father: this.state.father,
        mother: this.state.mother,
        gender: this.state.gender,
        dob: this.state.dob,
        dod: this.state.dod,
      };

      this.doAction({ person: newPerson });
      this.handleClose();
    }
  }

  allMail() {
    return this.props.allPerson.filter(rec => {
      if (rec.gender === 'MALE') {
        return true;
      }
    });
  }

  allFemail() {
    return this.props.allPerson.filter(rec => {
      if (rec.gender === 'FEMALE') {
        return true;
      }
    });
  }

  doAction(data) {
    const { dispatch } = this.props;
    if (this.props.actionType === 'UPDATE_PERSON') {
      data.person.uuid = this.props.person.uuid;
      dispatch(updatePersons(data));
    } else {
      dispatch(addPersons(data));
    }
  }
 
  render() {
    const { classes } = this.props;
    return (
      <div>
        <Dialog
          open={this.state.open}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="alert-dialog-title">{this.props.dialogTitle}</DialogTitle>
          <DialogContent>
            <TextField
              name="name"
              value={this.state.name}
              onChange={this.onChange}
              label="Name"
              style={{ width: '100%' }}
            />
            {/* <TextField
              name="description"
              value={this.state.description}
              onChange={this.onChange}
              label="Comments"
              style={{ width: '100%' }}
              multiline
              rows={5}
            /> */}
            <TextField
              name="dob"
              value={this.state.dob}
              onChange={this.onChange}
              type="date"
              label="Date of Birth"
              style={{ width: '100%' }}
            />
            <TextField
              name="dod"
              value={this.state.dod}
              onChange={this.onChange}
              type="date"
              label="Date of Death :("
              style={{ width: '100%' }}
            />
            <FormControl className={classes.formControl}
              style={{ width: '100%' }}>
              <InputLabel htmlFor="age-simple">Father</InputLabel>
              <Select
                value={this.state.father}
                onChange={this.onChange}
                inputProps={{
                  name: 'father',
                  id: 'father-simple',
                }}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                {
                  this.allMail().map((relation) =>
                    <MenuItem key={relation.uuid} value={relation.uuid}>{relation.name} ({relation.house.name})</MenuItem>
                  )
                }
              </Select>
            </FormControl>
            <FormControl className={classes.formControl}
              style={{ width: '100%' }}>
              <InputLabel htmlFor="age-simple">Mother</InputLabel>
              <Select
                value={this.state.mother}
                onChange={this.onChange}
                inputProps={{
                  name: 'mother',
                  id: 'mother-simple',
                }}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                {
                  this.allFemail().map((relation) =>
                    <MenuItem key={relation.uuid} value={relation.uuid}>{relation.name} ({relation.house.name})</MenuItem>
                  )
                }
              </Select>
            </FormControl>
            <FormGroup row>
            <FormControl component="fieldset" className={classes.formControl}>
              {/* <FormLabel component="legend">Gender</FormLabel> */}
              <RadioGroup
                aria-label="gender"
                name="gender"
                onChange={this.onChange}
                className={classes.group}
                value={this.state.gender}
              >
                <FormControlLabel value="MALE" control={<Radio color="primary" />} label="Male" />
                <FormControlLabel value="FEMALE" control={<Radio color="primary" />} label="Female" />
              </RadioGroup>
              </FormControl>
              </FormGroup>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={this.handleClose}
              className={classes.button}
              variant="raised"
              color="default"
              disabled={this.state.uploading}
            >
              <div style={{ paddingTop: '3px' }}>Cancel</div>
              <Cancel className={classes.rightIcon} />
            </Button>
            <Button
              onClick={this.handleSubmit}
              className={classes.buttonSave}
              variant="raised"
              color="primary"
              disabled={this.state.uploading}
            >
              <div style={{ paddingTop: '3px' }}>Save</div>
              <Save className={classes.rightIcon} />
            </Button>,
            </DialogActions>
        </Dialog>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { HomeReducer } = state;
  const {
    houses, allPerson,
  } = HomeReducer;
  return {
    houses, allPerson,
  };
}

CreatePerson.propTypes = {
  dispatch: PropTypes.func.isRequired,
  marker: PropTypes.object,
  open: PropTypes.bool,
  modalClosed: PropTypes.func,
  person: PropTypes.object,
  actionType: PropTypes.string.isRequired,
  dialogTitle: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
  handleResponse: PropTypes.func.isRequired,
};

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    backgroundColor: '#fff',
  },
  buttonSave: {
    margin: theme.spacing.unit,
    backgroundColor: 'purple',
    '&:hover': {
      backgroundColor: '#033050',
    },
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
  group: {
    margin: `${theme.spacing.unit}px 0`,
    display: 'inline'
  },
});

export default compose(
  withStyles(styles),
  connect(mapStateToProps),
)(CreatePerson);
