import React from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Delete from '@material-ui/icons/Delete';
import Cancel from '@material-ui/icons/Cancel';
import { deleteWapoint } from './HomeActions';

class DeleteHouseAlert extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
    this.handleClose = this.handleClose.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.open !== this.props.open) {
      this.setState({
        open: nextProps.open,
      });
    }
  }

  handleClose() {
    this.setState({ open: false });
    this.props.handleCloseDelete();
  }

  handleDelete() {
    const { dispatch } = this.props;
    const houseId = this.props.house && this.props.house.uuid;
    dispatch(deleteWapoint(houseId));
    this.handleClose();
    this.props.closeHouseFrom();
  }

  render() {
    const { classes } = this.props; 
    
    return (
      <div>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"Delete House"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
            Are you sure you want to delete this House? All associated members will be deleted.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} className={classes.button} variant="raised" color="default">
              <div style={{ paddingTop: '3px' }}>Cancel</div>
              <Cancel className={classes.rightIcon} />
            </Button>
            <Button onClick={this.handleDelete} className={classes.buttonSecondary} variant="raised" color="secondary">
              <div style={{ paddingTop: '3px' }}>Delete</div>
              <Delete className={classes.rightIcon} />
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

DeleteHouseAlert.propTypes = {
  dispatch: PropTypes.func.isRequired,
  handleCloseDelete: PropTypes.func.isRequired,
  house: PropTypes.object.isRequired,
  closeHouseFrom: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  classes: PropTypes.object.isRequired,
};

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    backgroundColor: '#fff',
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  buttonSecondary: {
    margin: theme.spacing.unit,
    backgroundColor: '#dc3545',
    '&:hover': {
      backgroundColor: '#dc1f31',
    },
  },
});

export default withStyles(styles)(DeleteHouseAlert);
