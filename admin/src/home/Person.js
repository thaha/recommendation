import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import ShoppingCart from '@material-ui/icons/ShoppingCart';
import IconButton from '@material-ui/core/IconButton';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import Edit from '@material-ui/icons/Edit';
import Snackbar from '@material-ui/core/Snackbar';

import TimeAgo from 'react-timeago';

import CreatePerson from './CreatePerson';
import DeletePersonAlert from './DeletePersonAlert';
import Buy from './Buy';

import moment from 'moment';


const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  stylePrimary: {
    color: 'purple',
  },
  styleSecondary: {
    color: '#dc3545',
  },
  blueCard: {
    maxWidth: 345,
    borderTop: '5px solid blue',
  },
  pinkCard: {
    maxWidth: 345,
    borderTop: '5px solid darkorange',
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    marginBottom: 16,
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  box: { 
    padding: 15,
  }
});

class Person extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      person: this.props.person,
      openPersonView: false,
      openUpdatePersonView: false,
      openDeleteConfirm: false,
      snackBarState: false,
      snackBarMessage: '',
    };
    this.handleonClickPerson = this.handleonClickPerson.bind(this);
    this.onDetailViewClose = this.onDetailViewClose.bind(this);
    this.handleEditPerson = this.handleEditPerson.bind(this);
    this.handleCloseUpdateView = this.handleCloseUpdateView.bind(this);
    this.handleDeletePerson = this.handleDeletePerson.bind(this);
    this.handleBuyProduct = this.handleBuyProduct.bind(this);
    this.handleCloseDelete = this.handleCloseDelete.bind(this);
    this.handleCloseBuy = this.handleCloseBuy.bind(this);
    this.stopMouseClick = this.stopMouseClick.bind(this);
    this.handleSnackbarClose = this.handleSnackbarClose.bind(this);
    this.handleResponse = this.handleResponse.bind(this);
  }


  componentWillReceiveProps(nextProps) {
    this.setState({
      person: nextProps.person,
    });
  }

  stopMouseClick(e) {
    e.stopPropagation();
  }

  handleSnackbarClose() {
    this.setState({
      snackBarState: false,
      snackBarMessage: '',
    });
  }

  handleResponse(message, open) {
    this.setState({
      snackBarState: open,
      snackBarMessage: message,
    });
  }

  handleonClickPerson() {
    this.setState({
      openPersonView: true,
    });
  }

  handleEditPerson(e) {
    this.stopMouseClick(e);
    this.setState({
      openUpdatePersonView: true,
    });
  }

  handleCloseUpdateView() {
    this.setState({
      openUpdatePersonView: false,
      openBuyModal: false,
    });
  }

  handleDeletePerson(e) {
    this.stopMouseClick(e);
    this.setState({
      openDeleteConfirm: true,
    });
  }

  handleBuyProduct(e) {
    this.stopMouseClick(e);
    this.setState({
      openBuyModal: true,
    });
  }

  onDetailViewClose(status) {
    this.setState({
      openPersonView: status,
    });
  }

  _getTimeAgoFormatted(value, unit, suffix, date, formatter) {
    if (unit === 'second' || unit === 'minute') {
      return 'moments ago';
    }
    return TimeAgo.defaultProps.formatter(value, unit, suffix, date);
  }

  _getActivityTime(activityTime) {
    let someDate = new Date(activityTime);
    someDate = someDate.getTime();
    return <TimeAgo formatter={this._getTimeAgoFormatted} date={parseInt(someDate, 10)} />;
  }

  handleCloseDelete() {
    this.setState({
      openDeleteConfirm: false,
    });
  }

  handleCloseBuy() {
    this.setState({
      openBuyModal: false,
    });
  }

  getAge(person) {
    if (person.dob != '') {
      if (person.dod != '') {
        return moment(person.dod).diff(moment(person.dob), 'years');
      } else {
        return moment().diff(moment(person.dob), 'years');
      }
    } else {
      return 'Unknown'
    }
  }

  getPurchases(person) {
    return person.PURCHASED.map((elem) => {
      return elem.name;
    }).join(', ');
  }

  render() {
    const { classes } = this.props; 

    return (
      <div className={classes.box}>
        <Card className={ this.state.person.gender === 'MALE' ? classes.blueCard : classes.pinkCard }>
          <CardContent>
            <Typography variant="headline" component="h2">
              {this.state.person.name}
            </Typography>
              <Typography className={classes.title} color="textSecondary">
              {this.state.person.description}
            </Typography>
              <Typography className={classes.pos} color="textSecondary">
                DOB: {this.state.person.dob}
            </Typography>
            <Typography className={classes.pos} color="textSecondary">
              Father: {this.state.person.FATHER ? this.state.person.FATHER.name : '-'}
            </Typography>
              <Typography className={classes.pos} color="textSecondary">
              Mother: {this.state.person.MOTHER ? this.state.person.MOTHER.name : '-'}
            </Typography>
            <Typography className={classes.pos} color="textSecondary">
                DOD: {this.state.person.dod}
            </Typography>
            <Typography className={classes.pos} color="textSecondary">
              AGE: {this.getAge(this.state.person)}
            </Typography>
            {
              this.state.person.PURCHASED && this.state.person.PURCHASED.map((product) =>
                <Typography key={product.uuid} className={classes.pos} color="textSecondary">
                  <ShoppingCart /> {product.name}
                </Typography>
              )
            }
          </CardContent>
          <CardActions>
            <IconButton onClick={this.handleBuyProduct} className={classes.button} aria-label="Buy" color="primary">
              <ShoppingCart/>
            </IconButton>
            <IconButton onClick={this.handleEditPerson} className={classes.button} aria-label="Edit">
              <Edit />
            </IconButton>
            <IconButton onClick={this.handleDeletePerson} className={classes.button} aria-label="Delete" color="secondary">
              <DeleteIcon className={classes.styleSecondary} />
            </IconButton>
          </CardActions>
        </Card>
        <CreatePerson
          open={this.state.openUpdatePersonView}
          modalClosed={this.handleCloseUpdateView}
          currentHouseId={this.props.house.uuid}
          person={this.props.person}
          dialogTitle="Update Person"
          actionType="UPDATE_PERSON"
          handleResponse={this.handleResponse}
        />
        <DeletePersonAlert
          open={this.state.openDeleteConfirm}
          handleCloseDelete={this.handleCloseDelete}
          person={this.props.person}
        />
        <Buy
          open={this.state.openBuyModal}
          handleCloseDelete={this.handleCloseBuy}
          modalClosed={this.handleCloseUpdateView}
          person={this.props.person}
          handleResponse={this.handleResponse}
        />
        <Snackbar
          open={this.state.snackBarState}
          message={this.state.snackBarMessage}
          autoHideDuration={3000}
          onRequestClose={this.handleSnackbarClose}
        />
      </div>
    );
  }
}

Person.propTypes = {
  classes: PropTypes.object.isRequired,
  person: PropTypes.object,
  house: PropTypes.object,
};


export default withStyles(styles)(Person);
