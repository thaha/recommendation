import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import MenuItem from '@material-ui/core/MenuItem';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import compose from 'recompose/compose';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Save from '@material-ui/icons/Save';
import Cancel from '@material-ui/icons/Cancel';
import ShoppingCart from '@material-ui/icons/ShoppingCart';
import {
  buyProduct, fetchProductRec
} from './HomeActions';

class Buy extends Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {};
    this.initState();
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      open: nextProps.open,
    });

    if (nextProps.open && !this.props.open) {
      const { dispatch } = this.props;
      dispatch(fetchProductRec(nextProps.person.uuid));
    }
  }

  initState() {
    this.state = {
      open: this.props.open,
      product: '',
    };
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleClose() {
    this.setState({ open: false });
    this.props.modalClosed(true);
    this.initState();
  }

  handleOpen() {
    this.setState({ open: true });
  }

  isFormValid() {
    let valid = true;

    if (!this.state.product === '') {
      valid = false;
    }
    
    return valid;
  }

  handleSubmit() {
    if (this.isFormValid()) {
      this.doAction({ person: this.props.person, product: {id: this.state.product}});
      this.handleClose();
    }
  }

  doAction(data) {
    const { dispatch } = this.props;
    dispatch(buyProduct(data));
  }
 
  render() {
    const { classes } = this.props;
    return (
      <div>
        <Dialog
          open={this.state.open}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="alert-dialog-title">Purchase an item</DialogTitle>
          <DialogContent>      
            {
              this.props.recommendations.length > 0 && 
              <div>
                <div>Recomendations</div>
                {
                this.props.recommendations.map((product) =>
                    <div key={product.uuid} className={classes.box}>
                    <Card>
                      <CardContent>
                        <Typography variant="headline" component="h2">
                          {product.name}
                        </Typography>
                        <Typography className={classes.title} color="textSecondary">
                          ${product.price}
                        </Typography>
                        <Typography className={classes.title} color="textSecondary">
                          Your {product.relation} {product.by.name} Purchased this item.
                        </Typography>
                      </CardContent>
                    </Card>
                  </div>
                )
              }
              </div>
            }
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="age-simple">Product</InputLabel>
              <Select
                value={this.state.product}
                onChange={this.onChange}
                inputProps={{
                  name: 'product',
                  id: 'product-simple',
                }}
              >
                {
                  this.props.products.map((product) =>
                    <MenuItem key={product.uuid} value={product.uuid}>{product.name} (${product.price})</MenuItem>
                  )
                }
              </Select>
            </FormControl>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={this.handleClose}
              className={classes.button}
              variant="raised"
              color="default"
              disabled={this.state.uploading}
            >
              <div style={{ paddingTop: '3px' }}>Cancel</div>
              <Cancel className={classes.rightIcon} />
            </Button>
            <Button
              onClick={this.handleSubmit}
              className={classes.buttonSave}
              variant="raised"
              color="primary"
              disabled={this.state.uploading}
            >
              <div style={{ paddingTop: '3px' }}>Buy</div>
              <ShoppingCart className={classes.rightIcon} />
            </Button>,
            </DialogActions>
        </Dialog>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { HomeReducer } = state;
  const {
    products, recommendations
  } = HomeReducer;
  return {
    products, recommendations
  };
}

Buy.propTypes = {
  dispatch: PropTypes.func.isRequired,
  open: PropTypes.bool,
  modalClosed: PropTypes.func,
  person: PropTypes.object,
  classes: PropTypes.object.isRequired,
  handleResponse: PropTypes.func.isRequired,
};

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    backgroundColor: '#fff',
  },
  buttonSave: {
    margin: theme.spacing.unit,
    backgroundColor: 'purple',
    '&:hover': {
      backgroundColor: '#033050',
    },
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
  group: {
    margin: `${theme.spacing.unit}px 0`,
    display: 'inline'
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    marginBottom: 16,
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  box: {
    padding: 15,
  }
});

export default compose(
  withStyles(styles),
  connect(mapStateToProps),
)(Buy);
