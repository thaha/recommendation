import {
  GET_HOUSES,
  ADD_HOUSES,
  UPDATE_HOUSE,
  DELETE_HOUSE,
  GET_PERSONS,
  GET_ALL_PERSON,
  ADD_PERSONS,
  GET_OTHER_HOUSES,
  UPDATE_PERSONS,
  REQUEST_STARTED,
  CATCH_ERROR,
  DELETE_PERSON,
  BUY,
  PRODUCTS,
  RECOMMENDATIONS,
} from './HomeActions';

const initialState = {
  isFetching: false,
  lastUpdated: 0,
  error: false,
  errors: null,
  errorMessage: '', // remove & use errors
  products: [],
  houses: [],
  persons: [],
  recommendations: [],
  otherHouses: [], 
  allPerson: [],
  actionResponse: {},
};

function updateObjectInArray(array, element) {
  return array.map((item) => {
    if (item.uuid !== element.uuid) {
      return item;
    }
    return element;
  });
}

function deleteObjectInArray(array, element) {
  let itemIndex = -1;
  array.map((item, index) => {
    if (item.uuid === element.uuid) {
      itemIndex = index;
    }
  });
  const newArray = array.slice();
  if (itemIndex >= 0) {
    newArray.splice(itemIndex, 1);
  }
  return newArray;
}

export default function DrawerReducer(state = initialState, action) {
  switch (action.type) {
    case CATCH_ERROR: {
      return Object.assign({}, state, {
        isFetching: false,
        lastUpdated: Date.now(),
        error: true,
        errorMessage: action.errorMessage,
      });
    }
    case REQUEST_STARTED: {
      return Object.assign({}, state, {
        isFetching: true,
        actionResponse: action.actionResponse,
      });
    }
    case CATCH_ERROR: {
      return Object.assign({}, state, {
        isFetching: false,
        lastUpdated: Date.now(),
        error: true,
        errorMessage: action.errorMessage,
        actionResponse: action.actionResponse,
      });
    }

    case PRODUCTS: {
      return Object.assign({}, state, {
        isFetching: false,
        products: action.products,
        lastUpdated: Date.now(),
        error: false,
      });
    }

    case RECOMMENDATIONS: {
      return Object.assign({}, state, {
        isFetching: false,
        recommendations: action.recommendations,
        lastUpdated: Date.now(),
        error: false,
      });
    }

    case BUY: {
      return Object.assign({}, state, {
        isFetching: false,
        lastUpdated: Date.now(),
        error: false,
      });
    }

    case GET_HOUSES: {
      return Object.assign({}, state, {
        isFetching: false,
        houses: action.houses,
        lastUpdated: Date.now(),
        error: false,
      });
    }
    case ADD_HOUSES: {
      return Object.assign({}, state, {
        isFetching: false,
        houses: [...state.houses, action.house],
        lastUpdated: Date.now(),
        error: false,
        houseAdded: true,
        actionResponse: action.actionResponse,
      });
    }
    case UPDATE_HOUSE: {
      return Object.assign({}, state, {
        isFetching: false,
        houses: updateObjectInArray(state.houses, action.house),
        lastUpdated: Date.now(),
        error: false,
        actionResponse: action.actionResponse,
      });
    }
    case DELETE_HOUSE: {
      return Object.assign({}, state, {
        isFetching: false,
        houses: deleteObjectInArray(state.houses, action.house),
        lastUpdated: Date.now(),
        error: false,
        actionResponse: action.actionResponse,
      });
    }
    case GET_PERSONS: {
      return Object.assign({}, state, {
        isFetching: false,
        lastUpdated: Date.now(),
        error: false,
        persons: action.persons,
      });
    }
    case GET_ALL_PERSON: {
      return Object.assign({}, state, {
        isFetching: false,
        lastUpdated: Date.now(),
        error: false,
        allPerson: action.allPerson,
      });
    }
    case ADD_PERSONS: {
      return Object.assign({}, state, {
        isFetching: false,
        persons: [...state.persons, action.person],
        lastUpdated: Date.now(),
        error: false,
        actionResponse: action.actionResponse,
      });
    }
    case GET_OTHER_HOUSES: {
      return Object.assign({}, state, {
        isFetching: false,
        otherHouses: action.otherHouses,
        lastUpdated: Date.now(),
        error: false,
      });
    }
    case UPDATE_PERSONS: {
      return Object.assign({}, state, {
        isFetching: false,
        persons: updateObjectInArray(state.persons, action.person),
        lastUpdated: Date.now(),
        error: false,
        actionResponse: action.actionResponse,
      });
    }
    case DELETE_PERSON: {
      return Object.assign({}, state, {
        isFetching: false,
        persons: deleteObjectInArray(state.persons, action.person),
        lastUpdated: Date.now(),
        error: false,
        actionResponse: action.actionResponse,
      });
    }
    default:
      return state;
  }
}
