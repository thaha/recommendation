import React from 'react';

import {
    withScriptjs,
    withGoogleMap,
    GoogleMap,
    Marker,
    InfoWindow,
} from 'react-google-maps';


const { SearchBox } = require('react-google-maps/lib/components/places/SearchBox');


/* global google*/


const INPUT_STYLE = {
  boxSizing: 'border-box',
  MozBoxSizing: 'border-box',
  border: '1px solid transparent',
  width: '240px',
  height: '32px',
  marginTop: '27px',
  padding: '0 12px',
  borderRadius: '1px',
  boxShadow: '0 2px 6px rgba(0, 0, 0, 0.3)',
  fontSize: '14px',
  outline: 'none',
  textOverflow: 'ellipses',
};

function filterHouses(data) {
  return data;
}

const MapWithAMarker = withScriptjs(withGoogleMap(props =>
  <GoogleMap
    defaultZoom={16}
    onRightClick={(e) => props.onRightClick(e, props.newHouse)}
    center={props.center}
    // center={{ lat: 11.765170276781578, lng: 75.59335697265146 }}

  >
    <SearchBox
      ref={props.onSearchBoxMounted}
      bounds={props.bounds}
      controlPosition={google.maps.ControlPosition.TOP_LEFT}
      onPlacesChanged={props.onPlacesChanged}
      inputPlaceholder="Search"
      inputStyle={INPUT_STYLE}
    >
      <input
        type="text"
        placeholder="Search"
        style={{
          boxSizing: 'border-box',
          border: '1px solid transparent',
          width: '240px',
          height: '28px',
          marginTop: '10px',
          padding: '0 12px',
          borderRadius: '3px',
          boxShadow: '0 2px 6px rgba(0, 0, 0, 0.3)',
          fontSize: '14px',
          outline: 'none',
          textOverflow: 'ellipses',
        }}
      />
    </SearchBox>
    {props.houses && props.houses.length > 0 &&
    <div className="">
      { filterHouses(props.houses).map((house, index) =>
        <Marker
          key={index}
          draggable={house.editMode}
          icon={{
            url: 'house_ico.png',
            size: new google.maps.Size(40, 40),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(40, 40),
          }}
          onClick={(e) => props.onMarkerClick(e, house)}
          onDragEnd={(e) => props.onAddMarkerChange(e, house)}
          onMouseOver={(e) => props.onMarkerHover(e, house)}
          onMouseOut={(e) => props.onMarkerHoverEnd(e, house)}
          position={house}
        >
          <InfoWindow>
            <div className="customStyle-02">{house.name}</div>
          </InfoWindow>
        </Marker>
                )}
    </div>
        }
    {props.houses && !props.newHouse.hideAdd &&
    <Marker
      draggable
      icon={{
        url: 'add_new_house.png',
        size: new google.maps.Size(50, 50),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(20, 40),
        scaledSize: new google.maps.Size(50, 50),
      }}
      animation={google.maps.Animation.DROP}
      onDragEnd={(e) => props.onAddMarkerChange(e, props.newHouse)}
      onClick={(e) => props.onAddMarkerClick(e, props.newHouse)}
      position={props.newHouse}
    >
      {props.houses.length === 0 &&
      <InfoWindow>
        <div className="customStyle-02">Place the marker and click to add new houses</div>
      </InfoWindow>
                    }
    </Marker>
    }
  </GoogleMap>
));

export default MapWithAMarker;
