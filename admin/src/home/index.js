import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Drawer from '@material-ui/core/Drawer';
import MenuItem from '@material-ui/core/MenuItem';
import AppBar from '@material-ui/core/AppBar';
import { withStyles } from '@material-ui/core/styles';
import compose from 'recompose/compose';

import { connect } from 'react-redux';
import './Home.css';

import { HousesContainer } from './HousesContainer';

export class Home extends Component {
  constructor(props) {
    super(props);
    this.state = { open: true };
  }
  componentDidMount() {
    const { dispatch } = this.props;
  }

  render() {
    const { classes } = this.props;
    return (
<div> 
    {this.props.isFetching && <div className="home-overlay"> </div>} 
        <main role="main" className="col-sm-9 col-md-12 pt-3">
          <section className="wrapperAround1">
            <div >
              {this.state &&
                <HousesContainer {...this.props} />
              }
            </div>
          </section>
        </main>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { HomeReducer } = state;
  const {
    isFetching,
    error,
    houses,
    success,
  } = HomeReducer;
  return {
    isFetching,
    error,
    houses,
    success,
  };
}

Home.propTypes = {
  dispatch: PropTypes.func.isRequired,
  isFetching: PropTypes.bool,
  classes: PropTypes.object.isRequired,
};

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  buttonSave: {
    margin: theme.spacing.unit,
    backgroundColor: 'purple',
    '&:hover': {
      backgroundColor: '#033050',
    },
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
})

export default compose(withStyles(styles), connect(mapStateToProps))(Home);
