import axios from 'axios';
import { BASE_URL_API, S3_URL, IMAGE_SERVER_URL } from '../App';

export const GET_HOUSES = 'GET_HOUSES';
export const ADD_HOUSES = 'ADD_HOUSES';
export const UPDATE_HOUSE = 'UPDATE_HOUSE';
export const DELETE_HOUSE = 'DELETE_HOUSE';

export const GET_PERSONS = 'GET_PERSONS';
export const GET_ALL_PERSON = 'GET_ALL_PERSON';
export const ADD_PERSONS = 'ADD_PERSONS';
export const GET_OTHER_HOUSES = 'GET_OTHER_HOUSES'
export const UPDATE_PERSONS = 'UPDATE_PERSONS';
export const REQUEST_STARTED = 'REQUEST_STARTED'
export const CATCH_ERROR = 'CATCH_ERROR'
export const DELETE_PERSON = 'DELETE_PERSON'

export const BUY = 'BUY'
export const PRODUCTS = 'PRODUCTS'
export const RECOMMENDATIONS = 'RECOMMENDATIONS'

/* globals localStorage*/

function config() {
  return {
    headers: {
      Authorization: localStorage.accesstoken,
    },
  }
};

function requestStarted(){
  return {
    type: REQUEST_STARTED,
    isFetching: true,
    lastUpdated: Date.now(),
    error: false,
    actionResponse:{},
  };
}

function catchError(error){
  return {
    type: CATCH_ERROR,
    isFetching: false,
    lastUpdated: Date.now(),
    error: true,
    errorMessage: error.message,
    actionResponse:{},
  };
}


function reciveHouses(houses) {
  return {
    type: GET_HOUSES,
    isFetching: false,
    lastUpdated: Date.now(),
    error: false,
    houses,
  };
}

function recivePersons(persons) {
  return {
    type: GET_PERSONS,
    isFetching: false,
    lastUpdated: Date.now(),
    error: false,
    persons,
  };
}

function reciveAllPerson(data) {
  return {
    type: GET_ALL_PERSON,
    isFetching: false,
    lastUpdated: Date.now(),
    error: false,
    allPerson: data,
  };
}

function addHouseAction(house) {
  return {
    type: ADD_HOUSES,
    isFetching: false,
    lastUpdated: Date.now(),
    error: false,
    house,
    actionResponse:{
      success: true,
      message: 'House Added',
    },
  };
}

function updateHouseAction(house) {
  return {
    type: UPDATE_HOUSE,
    isFetching: false,
    lastUpdated: Date.now(),
    error: false,
    house,
    actionResponse:{
      success: true,
      message: 'House Updated',
    },
  };
}

function deleteHouseAction(house) {
  return {
    type: DELETE_HOUSE,
    isFetching: false,
    lastUpdated: Date.now(),
    error: false,
    house,
    actionResponse:{
      success: true,
      message: 'House Deleted',
    },
  };
}

function addPersonAction(person) {
  return {
    type: ADD_PERSONS,
    isFetching: false,
    lastUpdated: Date.now(),
    error: false,
    person,
    actionResponse:{
      success: true,
      message: 'Person Created',
    },
  };
}

function updatePersonAction(person) {
  return {
    type: UPDATE_PERSONS,
    isFetching: false,
    lastUpdated: Date.now(),
    error: false,
    person,
    actionResponse:{
      success: true,
      message: 'Person Updated',
    },
  };
}

function buyAction(person) {
  return {
    type: BUY,
    isFetching: false,
    lastUpdated: Date.now(),
    error: false,
    person,
    actionResponse:{
      success: true,
      message: 'Purchased',
    },
  };
}

function getProductsAction(products) {
  return {
    type: PRODUCTS,
    isFetching: false,
    lastUpdated: Date.now(),
    error: false,
    products,
  };
}

function getProductsRecAction(products) {
  return {
    type: RECOMMENDATIONS,
    isFetching: false,
    lastUpdated: Date.now(),
    error: false,
    recommendations: products,
  };
}


function deletePersonAction(person){
  return {
    type: DELETE_PERSON,
    isFetching: false,
    lastUpdated: Date.now(),
    error: false,
    person,
    actionResponse:{
      success: true,
      message: 'Person Deleted',
    },
  };
}

export function fetchProducts() {
  return (dispatch) => {
    dispatch(requestStarted());
    const url = `${BASE_URL_API}/product`;
    axios.get(url, config()).then((res) => {
      dispatch(getProductsAction(res.data.data));
    }).catch(e => {
      dispatch(catchError(e));
    });
  };
}

export function fetchProductRec(personId) {
  return (dispatch) => {
    dispatch(requestStarted());
    const url = `${BASE_URL_API}/person/${personId}/recommendations`;
    axios.get(url, config()).then((res) => {
      dispatch(getProductsRecAction(res.data.data));
    }).catch(e => {
      dispatch(catchError(e));
    });
  };
}

export function buyProduct(data) {
  return (dispatch) => {
    dispatch(requestStarted());

    const url = `${BASE_URL_API}/product/${data.product.id}/buy`;
    axios.post(url, data, config()).then((res) => {
      dispatch(buyAction(res.data.data));
    }).catch(e => {
      dispatch(catchError(e));
    });
  };
}

export function fetchHouses() {
  return (dispatch) => {
    dispatch(requestStarted())
    const url = `${BASE_URL_API}/house`;
    axios.get(url, config()).then((res) => {
      dispatch(reciveHouses(res.data.data));
    }).catch(e => {
      dispatch(catchError(e));
    });
  };
}

export function addHouse(data) {
  return (dispatch) => {
    dispatch(requestStarted())
    const url = `${BASE_URL_API}/house`;
    axios.post(url, data, config()).then((res) => {
      dispatch(addHouseAction(res.data.data));
    }).catch(e => {
      dispatch(catchError(e));
    });
  };
}

export function updateHouse(data) {
  return (dispatch) => {
    dispatch(requestStarted())
    const url = `${BASE_URL_API}/house/${data.house.uuid}`;
    axios.put(url, data, config()).then((res) => {
      dispatch(updateHouseAction(res.data.data));
    }).catch(e => {
      dispatch(catchError(e));
    });
  };
}

export function deleteWapoint(id) {
  return (dispatch) => {
    dispatch(requestStarted())
    const url = `${BASE_URL_API}/house/${id}`;
    axios.delete(url, config()).then((res) => {
      dispatch(deleteHouseAction(res.data.data));
    }).catch(e => {
      dispatch(catchError(e));
    });
  };
}

export function fetchAllPerson() {
  return (dispatch) => {
    dispatch(requestStarted())
    const url = `${BASE_URL_API}/person`;
    axios.get(url, config()).then((res) => {
      dispatch(reciveAllPerson(res.data.data));
    }).catch(e => {
      dispatch(catchError(e));
    });
  };
}

export function fetchPersons(id) {
  return (dispatch) => {
    dispatch(requestStarted())
    const url = `${BASE_URL_API}/house/${id}/person`;
    axios.get(url, config()).then((res) => {
      dispatch(recivePersons(res.data.data)); 
    }).catch(e => {
      dispatch(catchError(e));
    });
  };
}

export function addPersons(data) {
  return (dispatch) => {
    dispatch(requestStarted())
    const url = `${BASE_URL_API}/house/${data.person.houseId}/person`;
    axios.post(url, data, config()).then((res) => {
      dispatch(addPersonAction(res.data.data));
      dispatch(fetchAllPerson());
    }).catch(e => {
      dispatch(catchError(e));
    });
  };
}

export function updatePersons(data) {
  return (dispatch) => {
    dispatch(requestStarted())
    const url = `${BASE_URL_API}/person/${data.person.uuid}`;
    axios.put(url, data, config()).then((res) => {
      dispatch(updatePersonAction(res.data.data));        
    }).catch(e => {
      dispatch(catchError(e));
    });
  };
}

export function deletePerson(id) {
  return (dispatch) => {
    dispatch(requestStarted())
    const url = `${BASE_URL_API}/person/${id}`;
    axios.delete(url, config()).then((res) => {
      dispatch(deletePersonAction(res.data.data));
    }).catch(e => {
      dispatch(catchError(e));
    });
  };
}
