import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Save from '@material-ui/icons/Save';
import Cancel from '@material-ui/icons/Cancel';

/*global google*/

import {
  fetchHouses,
  addHouse,
  updateHouse,
  deleteWapoint,
} from './HomeActions';

import { MAP_API_KEY, BASE_URL_API } from '../App';

import './Home.css';
import HouseDetails from './HouseDetails';
import MapWithAMarker from './MapWithAMarker';


export class HousesContainer extends Component {
  constructor(props) {
    super(props);
 
    this.state = {};
    this.setInitialState(props);
    this.handleHouseEdit = this.handleHouseEdit.bind(this);
    this.selectHouse = this.selectHouse.bind(this);
    this.onMarkerClick = this.onMarkerClick.bind(this);
    this.onMarkerHoverEnd = this.onMarkerHoverEnd.bind(this);
    this.onMarkerHover = this.onMarkerHover.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onAddMarkerClick = this.onAddMarkerClick.bind(this);
    this.onAddMarkerChange = this.onAddMarkerChange.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handlePlacesChanged = this.handlePlacesChanged.bind(this);
    this.handleSearchBoxMounted = this.handleSearchBoxMounted.bind(this);
    this.closeHouseFrom = this.closeHouseFrom.bind(this);
    this.onRightClick = this.onRightClick.bind(this);
  }

  setInitialState(props){
    let newHouseLocation = {
      lat: 58.969927,
      lng: 5.739307599999961,
    };

    if (props.houses && props.houses.length > 0) {
      const lastHouse = props.houses[props.houses.length - 1];

      newHouseLocation = {
        lat: lastHouse.lat + 0.001,
        lng: lastHouse.lng + 0.001,
      };
    }

    this.state = {
      showHouseDetails:false,
      showForm: false,
      
      center: { lat: 11.765170276781578, lng: 75.59335697265146 },
      errorName: '',
      houseFrom: {},
      newHouse: {
        houseId: null,
        name: '',
        description: '',
        lat: newHouseLocation.lat,
        lng: newHouseLocation.lng,
      },
      selectedHouse: {
        houseId: null,
        name: '',
        description: '',
        lat: newHouseLocation.lat,
        lng: newHouseLocation.lng,
      },
    }
  }

  updateCenter(props) {
    let newHouseLocation = {
      lat: 11.765170276781578, lng: 75.59335697265146
    };

    if (props.houses && props.houses.length > 0) {
      const lastHouse = props.houses[props.houses.length - 1];

      newHouseLocation = {
        lat: lastHouse.lat + 0.001,
        lng: lastHouse.lng + 0.001,
      };
    }

    this.setState({
      // center: newHouseLocation,
      errorName: '',
      houseFrom: {},
      newHouse: {
        houseId: null,
        name: '',
        description: '',
        lat: newHouseLocation.lat,
        lng: newHouseLocation.lng,
      },
      selectedHouse: {
        houseId: null,
        name: '',
        description: '',
        lat: newHouseLocation.lat,
        lng: newHouseLocation.lng,
      },
    });
  }

  handleSearchBoxMounted(searchBox) {
    this._searchBox = searchBox;
  }


  handlePlacesChanged() {
    const places = this._searchBox.getPlaces();
    console.log(places);
    
    const searchResults = places.map(place => ({
      position: {
        lat: place.geometry.location.lat(),
        lng: place.geometry.location.lng(),
      },
    }));

    if (searchResults.length > 0) {
      let newHouse = this.state.newHouse;
      newHouse = searchResults[0].position;

      this.setState({
        showForm: true,
        // center: newHouse,
        newHouse: newHouse,
      });
    }
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchHouses());
  }

  componentWillReceiveProps(nextProps) {
    const { dispatch } = this.props;
    if (nextProps.isFetching) {
      return;
    }
    this.updateCenter(nextProps);

    this.refreshSelectedHouse(nextProps);
  }
  
  refreshSelectedHouse(props) {
    if (!this.state.selectedHouse.uuid) {
      return;
    }

    for (let house of props.houses) {
      if (house.uuid === this.state.selectedHouse.uuid) {
        this.setState({ selectedHouse: house });
      }
    }
  }

  handleSubmit() {
    if (!this.state.houseFrom.name || this.state.houseFrom.name.length === '') {
      this.setState({
        errorName: 'Name is required',
      });
    } else if (this.state.houseFrom.name && this.state.houseFrom.name.length > 50) {
      this.setState({
        errorName: 'Name is too long (max 50 characters).',
      });
    } else {
      this.createHouse();
    }
  }

  createHouse() {
    const { dispatch } = this.props;
    
    let newHouse = this.state.houseFrom;

    let latlng = new google.maps.LatLng(newHouse.lat, newHouse.lng);
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({
      latLng: latlng,
    }, (results, status) => {
      // if (status === google.maps.GeocoderStatus.OK) {
      //   if (results[1]) {
      //     const result = results[1];
      //     newHouse.placeId = result.place_id;
      //     newHouse.formattedAddress = result.formatted_address;
      //     newHouse.types = result.types;
      //     newHouse.addressComponents = result.address_components;
      //   }
      // }
      
      newHouse.lat = newHouse.lat;
      newHouse.lng = newHouse.lng;

      if (this.state.houseFrom.uuid) {
        dispatch(updateHouse({ house: newHouse }));
        this.showForms(false, true);
      } else {
        newHouse.uuid = this.state.uuid;
        dispatch(addHouse({ house: newHouse }));
        this.showForms(false, false);
      }
      // this.showForms(false, false);
      this.setState({
        center: null
      });
    });
  }

  onMarkerClick(evt, house) {
    house.showInfo = true;
    let canShow = true;

    if (house.uuid === this.state.selectedHouse.uuid) {
      canShow = !this.state.showHouseDetails;
    }
    
    this.showForms(false, canShow, house);

    this.selectHouse(house);
  }

  showForms(addForm, personsList, house) {
    let houseFrom = {};

    this.state.selectedHouse.editMode = false;
    this.state.newHouse.hideAdd = false;

    if (this.state.houseFrom.originalLocation) {
      this.state.selectedHouse = {...this.state.houseFrom.originalLocation};      
    }

    if (addForm) {
      houseFrom = { ...house };
      houseFrom.originalLocation = house;
    }


    this.setState({
      showForm: addForm,
      houseFrom: houseFrom,
      showHouseDetails: personsList,
      errorName: null,
    });
  }

  onMarkerHoverEnd(evt, house) {
    if (house.uuid !== this.state.selectedHouse.uuid) {
      house.showInfo = false;
      this.setState({});
    }
  }

  onMarkerHover(evt, house) {
    house.showInfo = true;
    this.setState({});
  }

  handleHouseEdit() {
    this.showForms(true, false, this.state.selectedHouse);
    this.state.newHouse.hideAdd = true;
    this.state.selectedHouse.editMode = true;
  }

  onAddMarkerClick(evt, house) {
    house = {
      lat: evt.latLng.lat(),
      lng: evt.latLng.lng(),
    };
    
    house.stopBounce = true;

    this.showForms(!this.state.showForm, false, house);

    this.state.houseFrom = { ...house };
    this.selectHouse(house);
  }

  onAddMarkerChange(evt, house) {
    this.state.houseFrom.lat = evt.latLng.lat();
    this.state.houseFrom.lng = evt.latLng.lng();
    // this.state.houseFrom = {
    //   lat: evt.latLng.lat(),
    //   lng: evt.latLng.lng(),
    // };

    house.stopBounce = true;
    
    this.setState({
      showHouseDetails: false,
    });
  }

  onRightClick(evt, house) {
    const newHouse2 = {
      lat: evt.latLng.lat(),
      lng: evt.latLng.lng(),
    };
    this.setState({newHouse: newHouse2});

    return true;
  }

  selectHouse(house) {
    this.setState({
      selectedHouse: house,
    });
  }

  newLatLng() {
    if (this.state.houseFrom) {
      return `${this.state.houseFrom.lat}, ${this.state.houseFrom.lng}`;
    } else {
      return '';
    }
  }

  onChange(e) {
    this.state.houseFrom[e.target.name] = e.target.value;
    this.setState({
      errorName: '',
    });
  }

  closeHouseFrom() {
    this.setState({
      showHouseDetails: false,
    });
  }

  handleClose() {
    this.showForms(false, false, {});
  }

  render() {
    const { classes } = this.props;

    return (
      <div className="mapSection1">
        <div>
          <MapWithAMarker
            googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${MAP_API_KEY}&v=3.exp&libraries=geometry,drawing,places`}
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={<div style={{ height: `700px` }} />}
            mapElement={<div style={{ height: `100%` }} />}
            center={this.state.center}
            onMarkerClick={this.onMarkerClick}
            onMarkerHoverEnd={this.onMarkerHoverEnd}
            onMarkerHover={this.onMarkerHover}
            onAddMarkerClick={this.onAddMarkerClick}
            onAddMarkerChange={this.onAddMarkerChange}
            onRightClick={this.onRightClick}
            newHouse={this.state.newHouse}
            imageSize={this.imageSize}
            onPlacesChanged={this.handlePlacesChanged}
            onSearchBoxMounted={this.handleSearchBoxMounted}
            {...this.props}
          />
        </div>
        <div>
          {this.state.showForm &&
            <section className="wayp-dtl">
              <div className="card">
                <div className="card-header backgroundColor-primary">
                    <h5 className="card-title backgroundColor-primary align-title-w" >
                    {this.state.houseFrom.uuid ? 'Edit' : 'New'} House
                    </h5>
                </div>
                <div className="card-body">

              <div className="row">
                <div className="col-md-12">
                  <TextField
                    name="name"
                    value={this.state.houseFrom.name}
                    onChange={this.onChange}
                    label="Name"
                    style={{ width: '100%' }}
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <TextField
                      multiline
                      rows={5}
                    name="description"
                    value={this.state.houseFrom.description}
                    onChange={this.onChange}
                    label="Description"
                    style={{ width: '100%' }}
                  />
                </div>
              </div>
              <br />
              <div className="row">
                <div className="col-md-12">
                  <TextField
                    name="location"
                    value={this.newLatLng()}
                    label="Location"
                    style={{ width: '100%' }}
                    disabled
                  />
                </div>
                </div>
                <Button
                  onClick={this.handleSubmit}
                  className={classes.buttonSave}
                  variant="raised"
                  color="primary"
                >
                  <div style={{ paddingTop: '3px' }}>Save</div>
                  <Save className={classes.rightIcon} />
                </Button>
                <Button
                  onClick={this.handleClose}
                  className={classes.buttonDefault}
                  variant="raised"
                  color="default"
                >
                  <div style={{ paddingTop: '3px' }}>Cancel</div>
                  <Cancel className={classes.rightIcon} />
                </Button>
              </div>
            </div>

            </section>
          }
          {this.state.showHouseDetails &&
            <HouseDetails house={this.state.selectedHouse}
              houseId={this.state.uuid}
              handleHouseEdit={this.handleHouseEdit}
              closeHouseFrom={this.closeHouseFrom}
            />
          }
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { HomeReducer } = state;
  const {
    isFetching,
    houses,
    error,
    houseAdded,
    success,
  } = HomeReducer;
  return {
    isFetching,
    error,
    houseAdded,
    houses,
    success,
  };
}

HousesContainer.propTypes = {
  dispatch: PropTypes.func.isRequired,
  houses: PropTypes.array,
  classes: PropTypes.object.isRequired,
};

const styles = theme => ({
  buttonDefault: {
    margin: theme.spacing.unit,
    backgroundColor: '#fff',
  },
  buttonSave: {
    margin: theme.spacing.unit,
    backgroundColor: 'purple',
    '&:hover': {
      backgroundColor: '#033050',
    },
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
});

export default compose(
  connect(mapStateToProps),
  withStyles(styles),
)(HousesContainer);
