import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import compose from 'recompose/compose';

import { withStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import Edit from '@material-ui/icons//Edit';

import Person from './Person';
import CreatePerson from './CreatePerson';
import DeleteHouseAlert from './DeleteHouseAlert';
import { fetchAllPerson, fetchPersons, fetchProducts } from './HomeActions';

import AddIcon from '@material-ui/icons/Add';

import DeleteIcon from '@material-ui/icons/Delete';
import Button from '@material-ui/core/Button';

import IconButton from '@material-ui/core/IconButton';

class HouseDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openCreatePOIModal: false,
      persons: props.persons,
      house: props.house,
      openDeleteConfirm: false,
      snackBarState: false,
      snackBarMessage: '',
    };
    this.handleCreatePOI = this.handleCreatePOI.bind(this);
    this.handleClosePOIModal = this.handleClosePOIModal.bind(this);
    this.handleCloseDelete = this.handleCloseDelete.bind(this);
    this.handleDeleteHouse = this.handleDeleteHouse.bind(this);
    this.handleResponse = this.handleResponse.bind(this);
    this.handleSnackbarClose = this.handleSnackbarClose.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { dispatch } = this.props;
    if (nextProps.house.uuid !== this.props.house.uuid ) {
      dispatch(fetchAllPerson());
      dispatch(fetchProducts());
      dispatch(fetchPersons(nextProps.house.uuid));
    } else {
      this.initState(nextProps);
    }
  }

  componentWillUnmount() {
    this.setState({
      house: null,
      persons: [],
    });
  }

  initState(props) {
    this.setState({
      house: props.house,
      persons: props.persons,
    });
  }

  handleSnackbarClose() {
    this.setState({
      snackBarState: false,
      snackBarMessage: '',
    });
  }

  handleResponse(message, open) {
    this.setState({
      snackBarState: open,
      snackBarMessage: message,
    });
  }

  handleCreatePOI() {
    this.setState({
      openCreatePOIModal: true,
    });
  }

  handleDeleteHouse(e) {
    this.setState({
      openDeleteConfirm: true,
    });
  }

  handleClosePOIModal(status) {
    this.setState({
      openCreatePOIModal: !status,
    });
  }

  handleCloseDelete() {
    this.setState({
      openDeleteConfirm: false,
    });
  }

  render() {
    const { classes } = this.props;
    return (
      <section className="wayp-dtl">
        <CreatePerson
          open={this.state.openCreatePOIModal}
          modalClosed={this.handleClosePOIModal}
          currentHouseId={this.props.house.uuid}
          dialogTitle="Add New Member"
          actionType="CREATE_PERSON"
          handleResponse={this.handleResponse}
        />
        <div className="card">
          <div className="card-header">
            <div className="card-body">
              <div className="row">
                <div className="col-md-8">
                  <h5 className="card-title">
                    {this.props.house.name}
                  </h5>
                  {this.props.house.location && this.props.house.location.formattedAddress &&
                    <h6 className="card-subtitle mb-2 text-muted">{this.props.house.location.formattedAddress}</h6>}
                  {this.props.house.location && this.props.house.location.lat &&
                    <h6 className="card-subtitle mb-2 text-muted">{this.props.house.location.lat}, {this.props.house.location.lng}</h6>}
                  <p className="card-text">{this.props.house.description}</p>
                </div>
                <DeleteHouseAlert
                  open={this.state.openDeleteConfirm}
                  handleCloseDelete={this.handleCloseDelete}
                  house={this.props.house}
                  dispatch={this.props.dispatch}
                  closeHouseFrom={this.props.closeHouseFrom}
                />
                <div className="col-md-4 text-right">
                  <IconButton onClick={this.props.handleHouseEdit} aria-label="add" className={classes.buttonSmall}>
                    <Edit />
                  </IconButton>
                  <IconButton color="secondary" onClick={this.handleDeleteHouse} aria-label="add" className={classes.buttonSmallSecondary}>
                    <DeleteIcon />
                  </IconButton>
                </div>
              </div>
            </div>
          </div>
          <div className="card-body">
            <div className="card-deck">
              {
                this.props.persons && this.state.house &&
                this.props.persons.map((person, index) =>
                  <Person
                    key={index} person={person} house={this.state.house}
                  />)
              }
              {
                (!this.props.persons || (this.props.persons &&
                  this.props.persons.length === 0)) &&
                <div className="noPersonDiv">No Person Added</div>
              }
              <Button onClick={this.handleCreatePOI} variant="fab" color="primary" aria-label="add" className={classes.buttonCreate} title="Create New">
                <AddIcon />
              </Button>
            </div>
          </div>
          <Snackbar
            open={this.state.snackBarState}
            message={this.state.snackBarMessage}
            autoHideDuration={3000}
            onRequestClose={this.handleSnackbarClose}
          />
        </div>
      </section>
    );
  }
}

function mapStateToProps(state) {
  const { HomeReducer } = state;
  const {
    isFetching,
    persons,
    error,
    success,
  } = HomeReducer;

  return {
    isFetching,
    persons,
    error,
    success,
  };
}

HouseDetails.propTypes = {
  house: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  handleHouseEdit: PropTypes.func.isRequired,
  closeHouseFrom: PropTypes.func.isRequired,
  persons: PropTypes.array,
  houseId: PropTypes.string,
  classes: PropTypes.object.isRequired,
};

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    position: 'absolute',
    bottom: theme.spacing.unit * 4,
    right: theme.spacing.unit * 2,
  },
  buttonSmall: {
    margin: theme.spacing.unit,
  },
  buttonSmallSecondary: {
    margin: theme.spacing.unit,
    color: '#dc3545',
  },
  buttonCreate: {
    position: 'fixed',
    zIndex: 99,
    right: 20,
    bottom: 10,
    backgroundColor: 'purple',
    '&:hover': {
      backgroundColor: '#033050',
    }
  },
  stylePrimary: {
    color: 'purple',
  },
});


export default compose(
  withStyles(styles),
  connect(mapStateToProps),
)(HouseDetails);
