import {
  REQUEST_SIGNIN,
  SIGNIN_SUCCESS,
  SIGNIN_FAILURE,
  USER_LOGOUT,
} from './LoginAction';

const initialState = {
  isLoggedIn: false,
  isFetching: false,
  lastUpdated: 0,
  currentUser: {},
  user_system: {},
};

export default function LoginReducer(state = initialState, action) {
  switch (action.type) {
    case REQUEST_SIGNIN: {
      return Object.assign({}, state, {
        isLoggedIn: false,
        isLogging: true,
        didInvalidate: false,
      });
    }
    case SIGNIN_SUCCESS: {
      return Object.assign({}, state, {
        isLoggedIn: true,
        isLogging: false,
        didInvalidate: false,
        currentUser: action.currentUser,
        user_system: action.user_system,
        lastUpdated: Date.now(),
        success: true,
      });
    }
    case SIGNIN_FAILURE: {
      return Object.assign({}, state, {
        isLoggedIn: false,
        isLogging: false,
        didInvalidate: false,
        login_error: true,
      });
    }
    case USER_LOGOUT: {
      return Object.assign({}, state, {
        isLoggedIn: false,
        isLogging: false,
        didInvalidate: false,
        currentUser: {},
      });
    }
    default:
      return state;
  }
}
