import axios from 'axios';
import { hashHistory } from 'react-router';
import Alert from 'react-s-alert';
import { BASE_URL_API } from '../App';

export const REQUEST_SIGNIN = 'REQUEST_SIGNIN';
export const SIGNIN_SUCCESS = 'SIGNIN_SUCCESS';
export const SIGNIN_FAILURE = 'SIGNIN_FAILURE';
export const USER_LOGOUT = 'USER_LOGOUT';

/* globals localStorage */

function requestSignIn() {
  return {
    type: REQUEST_SIGNIN,
    error: false,
  };
}

function signInSuccess(response) {
  return {
    type: SIGNIN_SUCCESS,
    currentUser: {
      email: response.email,
      name: response.name,
      user_id: response.id,
    },
    user_system: response.system,
    error: false,
  };
}

function signInFailure() {
  return {
    type: SIGNIN_FAILURE,
    error: true,
  };
}

export function logOutUser() {
  return {
    type: USER_LOGOUT,
    error: false,
  };
}

export function loginUser(user) {
  return (dispatch) => {
    dispatch(requestSignIn());
    axios.post(`${BASE_URL_API}/users/sign_in`, {
      user: {
        username: user.userName,
        password: user.password,
      },
    }).then((response) => {
      if (!response) {
        Alert.error('Invalid email or password', {
          position: 'top-right',
          effect: 'slide',
          timeout: 3000,
        });
      } else {
        const data = response.data.data;
        localStorage.setItem('accesstoken', data.token);
        hashHistory.push('/');
      }
    });
  };
}

export function userLogOut() {
  return (dispatch) => {
    dispatch(logOutUser());
    localStorage.removeItem('accesstoken');
    hashHistory.push('/login');
  };
}
