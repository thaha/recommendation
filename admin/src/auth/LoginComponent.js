import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { loginUser } from './LoginAction';
import './Login.css';

export default class LoginComponent extends Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.buttonSubmit = this.buttonSubmit.bind(this);
    this.state = {
      userName: '',
      password: '',
      redirect: false,
      error: '',
    };
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
      error: '',
    });
  }


  validateEmail(email) {
    // eslint-disable-next-line
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    re.test(email);
    return true;
  }

  validate() {
    const { userName, password } = this.state;
    let errors = 0;
    if (!this.validateEmail(userName)) {
      this.setState({
        error: 'Please enter the email in correct format',
      });
      errors++;
    }
    if (userName.length === 0 || password.length === 0) {
      this.setState({ error: 'Please enter both email and password' });
      errors++;
    } 
    return errors === 0;
  }

  buttonSubmit(e) {
    e.preventDefault();
    const { dispatch } = this.props;
    if (this.validate()) {
      dispatch(loginUser(this.state));
    }
  }

  render() {
    return (
      <div className="login-signup login-body">
      <section id="loginSection" className="page-section-from-left active">
      <div id="loginForm" className="v-align-parent container-xs">
      <div className="v-align-content-mid">
        <div className="t-align-c logo-container">
        </div>
      <div>
          <form>
            <div className="form-row">
              <div className="input-wrapper icon-left bright icon-user">
                <input value={this.state.userName} onChange={this.onChange} name="userName" type="text" placeholder="Your E-mail" />
              </div>
            </div>
            <div className="form-row">
              <div className="input-wrapper icon-left bright icon-lock">
                <input value={this.state.password} onChange={this.onChange} name="password" type="password" placeholder="Password" />
              </div>
            </div>
            <div className="content">
              <div className="form-row m-t-2 form-footer">
                {this.state.error && <div className="erroralert erroralert-danger">{this.state.error}</div>}
                <input  type="submit" className="share-btn" defaultValue="Login" onClick={this.buttonSubmit}/>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    </section>
    </div>
    );
  }
}

LoginComponent.propTypes = {
  dispatch: PropTypes.func.isRequired,
};
