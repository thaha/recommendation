import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Alert from 'react-s-alert';
import LoginForm from './LoginComponent';


export function Login(props) {
  return (
    <div className="bg-page bg-mountains">
      <LoginForm dispatch={props.dispatch} />
      <Alert stack={{ limit: 3 }} />
    </div>
  );
}

function mapStateToProps(state) {
  const { LoginReducer } = state;
  const {
    isLogging,
    currentUser,
    user_system,
    error,
    success,
    login_error,
  } = LoginReducer;
  return {
    isLogging,
    currentUser,
    user_system,
    error,
    success,
    login_error,
  };
}

Login.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

export default connect(mapStateToProps)(Login);
