import React from 'react'; 
import { render } from 'react-dom';
import { Provider } from 'react-redux';
// import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import { createMuiTheme, MuiThemeProvider, } from '@material-ui/core/styles';
// import injectTapEventPlugin from 'react-tap-event-plugin/src/injectTapEventPlugin';
import App from './App';
import configureStore from './configureStore';
import './index.css';
// import injectTapEventPlugin from 'react-tap-event-plugin';
// Grab the state from a global variable injected into the server-generated HTML
const preloadedState = window.__PRELOADED_STATE__;

// import * as initFastClick from "react-fastclick";
// initFastClick(); 
import blue from '@material-ui/core/colors/blue';
const theme = createMuiTheme({
  palette: {
    primary: blue,
    textColor: '#303030',
    primary1Color: 'purple',
    pickerHeaderColor: 'purple',
    accent1Color: '#1A9481',
  },
  appBar: {
    height: 80,
  },
});
window.paceOptions = {
  restartOnRequestAfter: false,
  axios: {
    trackMethods: ['GET', 'POST', 'DELETE', 'PUT', 'PATCH']
  },
};

const store = configureStore(preloadedState);
// injectTapEventPlugin();

const muiTheme = createMuiTheme({
  palette: {
    textColor: '#303030',
    primary1Color: 'purple',
    pickerHeaderColor: 'purple',
    accent1Color: '#1A9481',
  },
  appBar: {
    height: 80,
  },
});
function App1() {
  return (
    <Provider store={store}>
      <MuiThemeProvider theme={theme}>
        <App />
      </MuiThemeProvider>
    </Provider>
  );
}
render(<App1 />, document.getElementById('root'));
