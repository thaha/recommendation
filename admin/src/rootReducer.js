import { combineReducers } from 'redux';
import LoginReducer from './auth/LoginReducer';
import DrawerReducer from './navigation/DrawerReducer';
import HomeReducer from './home/HomeReducer';

const rootReducer = combineReducers({
  LoginReducer,
  DrawerReducer,
  HomeReducer,
});

export default rootReducer;
