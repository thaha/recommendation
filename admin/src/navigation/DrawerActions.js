import axios from 'axios';
import { BASE_URL_API } from '../App';

export const REQUEST_USER_INFO = 'REQUEST_USER_INFO';
export const RECEIVE_USER_INFO = 'RECEIVE_USER_INFO';
export const RECEIVE_USER_INFO_ERROR = 'RECEIVE_USER_INFO_ERROR';
export const CHANGE_TITLE = 'CHANGE_TITLE';

/* globals localStorage*/

function requestUserInfo() {
  return {
    type: REQUEST_USER_INFO,
    error: false,
  };
}

function reciveUserInfo(currentUser) {
  return {
    type: RECEIVE_USER_INFO,
    isFetching: false,
    lastUpdated: Date.now(),
    error: false,
    currentUser,
  };
}

function reciveUserInfoError(response) {
  return {
    type: RECEIVE_USER_INFO_ERROR,
    error: true,
    errors: response,
  };
}

export function changeTitle(title) {
  return {
    type: CHANGE_TITLE,
    title,
  };
}

export function fetchUserInfo() {
  return (dispatch) => {
    dispatch(requestUserInfo());
    const config = {
      headers: {
        'Authentication-Token': localStorage.accesstoken,
      },
    };
    axios.get(`${BASE_URL_API}/users/current`, config)
    .then((response) => {
      dispatch(reciveUserInfo(response.data));
    })
    .catch((error) => {
      dispatch(reciveUserInfoError(error.response));
    });
  };
}
