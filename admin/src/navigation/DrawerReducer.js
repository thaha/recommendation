import {
  REQUEST_USER_INFO,
  RECEIVE_USER_INFO,
  RECEIVE_USER_INFO_ERROR,
  CHANGE_TITLE,
} from './DrawerActions';

const initialState = {
  isFetching: true,
  didInvalidate: false,
  currentUser: {},
  lastUpdated: 0,
  error: '',
  errors: null,
  title: 'Content Curation',
};
export default function DrawerReducer(state = initialState, action) {
  switch (action.type) {
    case REQUEST_USER_INFO: {
      return Object.assign({}, state, {
        isFetching: true,
        didInvalidate: false,
      });
    }
    case RECEIVE_USER_INFO: {
      return Object.assign({}, state, {
        isFetching: false,
        currentUser: action.currentUser,
        lastUpdated: Date.now(),
        error: false,
      });
    }
    case RECEIVE_USER_INFO_ERROR: {
      return Object.assign({}, state, {
        isFetching: false,
        error: true,
        errors: action.errors,
      });
    }
    case CHANGE_TITLE: {
      return Object.assign({}, state, {
        isFetching: false,
        title: action.title,
      });
    }
    default:
      return state;
  }
}
