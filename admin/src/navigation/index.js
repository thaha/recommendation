import React, { Component } from 'react';
import { connect } from 'react-redux';
import { hashHistory } from 'react-router';
import PropTypes from 'prop-types';
import { userLogOut } from '../auth/LoginAction';


class NavBar extends Component {
  constructor(props) {
    super(props);
    this.onLogout = this.onLogout.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
    this.handleChangeRequestDrawer = this.handleToggle.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleAccount = this.handleAccount.bind(this);
    this.state = {
      logged: false,
      open: false,
      width: '0',
      height: '0',
      menuValue: '',
    };
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }
  componentWillMount() {
    const { dispatch } = this.props;
    // dispatch(fetchUserInfo());
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onLogout(e) {
    e.preventDefault();
  }

  getRole() {
    return 'Admin';
    // const { currentUser } = this.props;
    // if (currentUser.roles.indexOf('admin') > -1) {
    //   return 'Admin';
    // } else if (currentUser.roles.indexOf('security_personnel') > -1) {
    //   return 'Security Personnel';
    // } else if (currentUser.roles.indexOf('staff') > -1) {
    //   return 'Staff';
    // }
    // return '';
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
    if (window.innerWidth < 750 && this.state.open) {
      this.setState({ open: false });
    }
  }

  handleChange(event, value) {
    this.setState({ menuValue: value });
  }

  handleToggle() {
    this.setState({ open: !this.state.open });
  }

  handleAccount(e) {
    e.preventDefault();
    hashHistory.push('/account');
  }

  handleHome(e) {
    e.preventDefault();
    hashHistory.push('/');
  }

  handleSettings(e) {
    e.preventDefault();
    hashHistory.push('/settings');
  }

  handleLogout() {
    const { dispatch } = this.props;
    dispatch(userLogOut());
  }

  handleChangeRequestDrawer(open) {
    this.setState({ open });
  }

  render() {
    const iconColor = 'purple';
    return (
      <div>
      </div>
    );
  }
}

NavBar.propTypes = {
  dispatch: PropTypes.func.isRequired,
  title: PropTypes.string,
  currentUser: PropTypes.object,
};

NavBar.defaultProps = {
  title: 'ContentCuration',
};


function mapStateToProps(state) {
  const { DrawerReducer } = state;
  const {
    currentUser,
    error,
    title,
  } = DrawerReducer;
  return {
    currentUser,
    error,
    title,
  };
}

export default connect(mapStateToProps)(NavBar);
