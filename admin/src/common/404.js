import React from 'react';
import logo from './logo-pd-2.png';

const style = {
  wrapperStyle: {
    backgroundColor: '#1ca0d9',
    borderWidth: '8px 0',
    borderStyle: 'solid',
    borderColor: '#000',
    textAlign: 'center',
    overflow: 'hidden',
    padding: '40px 20px 0',
    marginTop: '5%',
  },
  fontColor: { color: '#fff' },
  imgWidth: { maxWidth: '90%' },
};

export default function Page404() {
  return (
    <div style={style.wrapperStyle}>
      <div style={style.fontColor}>
        <img
          src={logo}
          alt="Almost there"
          style={style.imgWidth}
        />
        <h2>Cannot reach the page.</h2>
        <p>We cannot reach the page now, please try going back!</p>
      </div>
    </div>
  );
}
