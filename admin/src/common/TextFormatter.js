import React from 'react';
import PropTypes from 'prop-types';

const linkify = require('linkify-it')();
const tlds = require('tlds');

linkify.tlds(tlds);

export default class TextFormatter extends React.Component {
  constructor(props) {
    super(props);
    this.onMouseClick = this.onMouseClick.bind(this);
  }

  onMouseClick(e) {
    if (this.props.onClickHref) {
      this.props.onClickHref(e);
    }
  }

  format(str, options) {
    const matches = linkify.match(str) || [];
    let index = 0;
    const elements = [];
    const newlines = function (newLine, i) {
      const parts = newLine.split(/\n|\r/);

      parts.forEach((part, j) => {
        if (part) elements.push(part);
        if (j < parts.length - 1) elements.push(<br key={`${i}.${j}`} />);
      });
    };

    matches.forEach((match, i) => {
      const part = str.slice(index, match.index);
      const target = ('target' in options) ? options.target :
        ((/^http(s)?:/).test(match.url) ? '_blank' : null);
      const rel = ('rel' in options) ? options.rel :
        (target === '_blank' ? 'noopener noreferrer' : null);

      index = match.lastIndex;
      newlines(part, i);
      const aTag = <a key={i} target={target} rel={rel} href={match.url} onClick={this.onMouseClick}>{match.text}</a>;
      elements.push(aTag);
    });

    newlines(str.slice(index), matches.length);

    return elements;
  }

  render() {
    return (
      <span>{this.props.children && this.format(this.props.children, this.props)}</span>
    );
  }
}

TextFormatter.propTypes = {
  children: PropTypes.string.isRequired,
  onClickHref: PropTypes.func,
};
