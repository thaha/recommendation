import {
    createStore,
    applyMiddleware,
    compose,
  } from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from './rootReducer';

let middleware = [thunkMiddleware];

// Enable redux-logger only in development
if (process.env.NODE_ENV === 'development') {
  const { logger } = require('redux-logger');// eslint-disable-line global-require
  middleware = [...middleware, logger];
}
// eslint-disable-next-line no-underscore-dangle
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// Create Redux store with initial state
export default function configureStore(preloadedState) {
  return createStore(
    rootReducer,
    preloadedState,
    composeEnhancers(
      applyMiddleware(...middleware)
    )
  );
}
