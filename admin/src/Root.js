import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { connect } from 'react-redux';
import { hashHistory } from 'react-router';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';
import Snackbar from '@material-ui/core/Snackbar';
import { BASE_URL_API } from './App';

import {
  AppBar,
  IconButton,
  IconMenu,
  MenuItem,
} from '@material-ui/core';


// import LogOut from '@material-ui/icons/action/power-settings-new';
// import MoreVert from '@material-ui/icons/navigation/more-vert';

/* globals localStorage*/

axios.interceptors.response.use(
  (response) => response,
  (error) => {
    if (error.response.status === 401) {
      localStorage.removeItem('accesstoken');
      hashHistory.push('/login');
    }
  }
);

class Root extends Component {

  constructor(props) {
    super(props);
    this.state = {
      open: true,
      snackBarOpen: false,
      snackBarMessage: '',
    };
    this.handleToggle = this.handleToggle.bind(this);
    this.onLogout = this.onLogout.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
    this.handleSnackBarClose = this.handleSnackBarClose.bind(this);
  }

  componentWillMount() {
    this.checkAuthenticity();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.error) {
      this.setState({
        snackBarOpen: true,
        snackBarMessage: 'Something went wrong',
      });
    }
    if (nextProps.actionResponse && nextProps.actionResponse.success) {
      this.setState({
        snackBarOpen: true,
        snackBarMessage: nextProps.actionResponse.message,
      });
    }
  }

  handleToggle() {
    this.setState({ open: !this.state.open });
  }

  onLogout(e) {
    e.preventDefault();
  }

  handleLogout() {
    localStorage.removeItem('accesstoken');
    hashHistory.push('/login');
  }

  handleSnackBarClose() {
    this.setState({
      snackBarOpen: false,
    });
  }

  isLoggedIn(authenticationToken) {
    let result = false;
    const config = {
      headers: {
        Authorization: authenticationToken,
      },
    };
    axios.post(`${BASE_URL_API}/users/authorize`, {}, config)
      .then((response) => {
        if (!response.data.data.auth) {
          hashHistory.push('/login');
        }
      })
      .catch(() => {
        result = false;
      });
    return result;
  }

  checkAuthenticity() {
    const authenticationToken = localStorage.accesstoken;
    if (authenticationToken !== undefined) {
      this.isLoggedIn(authenticationToken);
    } else {
      hashHistory.push('/login');
    }
  }

  render() {
    return (
      <div>
        <div className="customPosition">
          <AppBar
            iconElementRight={
              <IconMenu
                onChange={this.handleChange}
                value={this.state.menuValue}
              >
                <MenuItem
                  value="2"
                  primaryText="Sign out"
                  onTouchTap={this.handleLogout}
                />
              </IconMenu>
            }
            onRightIconButtonTouchTap={this.handleLogout}
          />
        </div>
        {this.props.children}
        <Snackbar
          open={this.state.snackBarOpen}
          message={this.state.snackBarMessage}
          autoHideDuration={4000}
          onRequestClose={this.handleSnackBarClose}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { HomeReducer } = state;
  const {
    error,
    errorMessage,
    lastUpdated,
    actionResponse,
  } = HomeReducer;
  return {
    error,
    lastUpdated,
    errorMessage,
    actionResponse,
  };
}

Root.propTypes = {
  children: PropTypes.element.isRequired,
  error: PropTypes.bool,
  lastUpdated: PropTypes.number,
  errorMessage: PropTypes.string,
  actionResponse: PropTypes.object,
};

export default connect(mapStateToProps)(Root);
